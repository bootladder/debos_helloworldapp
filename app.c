#include "System.h"
#include <stdint.h>

uint8_t buf[100]; //optional message buf for checkpoint

/*************************************************************** 
   Application Header

   At the moment just a pointer located at 0x2000
   It points to the Application Function, and is called by the system
*/

#define APP_HEADER_SECTION __attribute__ ((section(".app_header")))
APP_HEADER_SECTION void (*apppointer)(void) = &app;

/*************************************************************** 
   Application Function

   This function is called by the system while(1)
   This Hello World App only calls the checkpoint function
*/

void app(void)
{
  //first dreference the address of checkpoint pointer
  uint32_t * addrptr = (uint32_t *)0x1FF0;
  uint32_t addr = *addrptr;

  //optional: for testing, check if the correct address is there
  if( addr == 0x01d8 || addr == 0x01d9 ){;}
  else {;}

  system_checkout_pointer_t * mycheckoutpointer = 
      (system_checkout_pointer_t*) addr;

  int z;
  for(uint32_t i=0;i<10000;i++)
    z+= i; 
    
  if(mycheckoutpointer)
    (mycheckoutpointer)(z,0,buf);
  return;
}
/***************************************************************/








/***************************************************************/
//dummy main.  use it to get the app to compile and not optimze stuff
int main(void)
{
  app();
  if( apppointer)
    return 1;
  return 0;
}
