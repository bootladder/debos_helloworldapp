#We try to detect the OS we are running on, and adjust commands as needed
ifeq ($(OSTYPE),cygwin)
  CLEANUP = rm -f
  MKDIR = mkdir -p
  TARGET_EXTENSION=.out
elseifeq ($(OSTYPE),msys)
    CLEANUP = rm -f
  MKDIR = mkdir -p
  TARGET_EXTENSION=.exe
else
  CLEANUP = rm -f
  MKDIR = mkdir -p
  TARGET_EXTENSION=.out
endif

UNITY_ROOT=./link_to_unity_root
HOST_C_COMPILER=gcc
CROSS_C_COMPILER=arm-none-eabi-gcc
CROSS_AR=arm-none-eabi-ar
CROSS_SIZE=arm-none-eabi-size
OBJCOPY = arm-none-eabi-objcopy
SIZE = arm-none-eabi-size

CFLAGS = -std=c99
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Werror
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition
CFLAGS += -Wmissing-prototypes
CFLAGS += -Wmissing-declarations
CFLAGS += -DUNITY_FIXTURES


CROSS_CFLAGS += -W -Wall --std=gnu99 -O0 -g
CROSS_CFLAGS += -fdata-sections -ffunction-sections
CROSS_CFLAGS += -funsigned-char -funsigned-bitfields
CROSS_CFLAGS += -mcpu=cortex-m0plus -mthumb
#CROSS_CFLAGS += -MD -MP -MT $(CONFIG)/$(*F).o -MF $(CONFIG)/$(@F).d

ifeq ($(CONFIG), Debug)
  CFLAGS += -g
endif

CROSS_LDFLAGS += -mcpu=cortex-m0plus -mthumb
CROSS_LDFLAGS += -Wl,--gc-sections
CROSS_LDFLAGS += -Wl,--script=./atsamd20j18.ld
CROSS_LDFLAGS += -Wl,-Map=output.map


TARGET_CROSS_PRODUCTION = DeBos_HelloWorldApp.elf
TARGET_CROSS_PRODUCTION_BIN = DeBos_HelloWorldApp.bin
TARGET_CROSS_PRODUCTION_LIB = libDeBos_HelloWorldApp_Cross_Production_Lib.a


SRC_FILES_PRODUCTION=\
  app.c \
  halStartup.c

SRC_FILES_PRODUCTION_MAIN=\

SRC_FILES_PRODUCTION_CROSSTARGET_ONLY=\


INC_DIRS=-I. -I../samd20_cmsis_headers -I../arm_cmsis_headers \
						 
all: production_cross clean

######################################################


######################################################
# CROSS COMPILES:  ONLY CREATE THE PRODUCTION EXECUTABLE

production_cross: $(SRC_FILES1)
	@echo Building production executable Cross compiled
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION) $(SRC_FILES_PRODUCTION_MAIN) \
				$(SRC_FILES_PRODUCTION_CROSSTARGET_ONLY) \
			$(CROSS_LDFLAGS) \
				 -o $(TARGET_CROSS_PRODUCTION)

	@echo $(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_BIN)
	@$(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_PRODUCTION_BIN)
	
######################################################
$(TARGET_CROSS_PRODUCTION_LIB): production_cross_objects
	@echo  -e '\n'$@'\n' Building production library for testing
	@echo $(CROSS_AR) rcs  $(TARGET_CROSS_PRODUCTION_LIB) *.o 
	@$(CROSS_AR) rcs  $(TARGET_CROSS_PRODUCTION_LIB) *.o 
######################################################
#create the object files for library
production_cross_objects: $(SRC_FILES1) 
	@echo -e '\n'$@'\n' Compiling tested production objects for TARGET 
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				 -c
######################################################
test_cross: $(TARGET_CROSS_TEST) clean
	@echo -e '\n'$@'\n' Deploying Test Executable on Target
	@sudo edbg -t atmel_cm0p -p -f $(TARGET_CROSS_TEST_BIN)

######################################################
$(TARGET_CROSS_TEST): $(TARGET_CROSS_PRODUCTION_LIB)
	@echo Building Cross-Compiled Test Executable
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(INC_DIRS_MOCK) \
			$(SRC_FILES_TEST) $(SRC_FILES_MOCK) \
				$(SRC_FILES_PRODUCTION_CROSSTARGET_ONLY) \
			$(CROSS_LDFLAGS)  \
			$(LIBRARY_ARGUMENTS_INCLUDE_TARGET_CROSS_PRODUCTION_LIB) \
			-lc
				-o $(TARGET_CROSS_TEST)
#$(TEST_TARGET): $(TEST_OBJS) $(MOCKS_OBJS)  $(PRODUCTION_CODE_START) $(TARGET_LIB) $(USER_LIBS) $(PRODUCTION_CODE_END) $(STDLIB_CODE_START)
#  $(SILENCE)echo Linking $@
#  $(SILENCE)$(LINK.o) -o $@ $^ $(LD_LIBRARIES)
#default:
#	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES1) -o $(TARGET1)
#	./$(TARGET1) -v

clean:
	$(CLEANUP) *.o



deploy:
	@sudo edbg -t atmel_cm0p -p -f $(TARGET_CROSS_PRODUCTION_BIN)
